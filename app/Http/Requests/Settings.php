<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use AppHelper;

Class Settings extends FormRequest {

    public function __construct() {
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $rules = array();
        if( $this->method() == "POST" ){
            $rules = [
                'key.*' => 'required'
            ];
        }
        return $rules;
    }

    public function messages() {
        $messages = [];
        $messages['key.*.required'] = 'Key field is required';

        return $messages;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = array();

        return $attributes;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator){
            // validate key duplicate or not
            if (count($validator->getMessageBag()) == 0){
                $key_count = array_count_values($this->get('key'));
                foreach($key_count as $count) {
                    if (count($this->get('key')) != count($key_count)){
                        $validator->errors()->add('key', 'Key can\'t be duplicate');
                    }
                }
            }
        });
    }

    public function response(array $error) {
    }
}
