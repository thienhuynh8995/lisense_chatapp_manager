<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{

    /**
     * Initialize public controller.
     *
     * @return null
     */
    public function __construct()
    {
        if (!empty(app('auth')->getDefaultDriver())) {
            $this->middleware('auth:' . app('auth')->getDefaultDriver());
            $this->middleware('role:' . get_guard('route'));
            if (request()->route() !== null && request()->route()->getName() != 'user.update_info') {
                $this->middleware('is_updated');
            }
        }
        config(['adminlte.logout_url' => route('logout')]);
        config(['adminlte.menu'=> [
            'MENU',
            [
                'text'        => 'Website',
                'url'         => route('website.index'),
                'icon'        => 'file',
            ],
            [
                'text'        => 'Account',
                'url'         => route('user.update'),
                'icon'        => 'user',
            ],
            // [
            //     'text'        => 'Upgrade',
            //     'url'         => route('upgrade'),
            //     'icon'        => 'wrench',
            //     'custom_class'=> 'hot-tag'
            // ]
        ]]);
    }
}
