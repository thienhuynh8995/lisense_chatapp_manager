<?php

namespace App\Http\Controllers\Frontend;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Frontend\BaseController;
use App\Http\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Helpers\EmailHelpers;
use App\Http\Models\Settings;
use App\Http\Models\Website;

class UpgradeController extends BaseController
{
    protected  $ruleForm = [
        'email' => 'required|email|unique:users',
        'name' => 'required',
        'password' => 'required|min:6',
        'confirm_password' => 'required_with:password|same:password'
    ];

    public function index() {
        if (request()->isMethod('POST')) {
            $user = Auth::user();
            $domain_id = request()->get('domain_id');
            $price = request()->get('price');
            $period = request()->get('period');
            $website = Website::where(['user_id'=>Auth::id(), 'id'=>$domain_id])->orderBy('id', 'DESC')->first();
            if (!empty($website)){
                $settingModel = new Settings();
                $emailSettingRaw = $settingModel->getPublicSettings('email');
                $upgrade_request = $website->upgrade_request;
                if ($upgrade_request && $website->expire_date !== null && Carbon\Carbon::parse($website->expire_date) < Carbon\Carbon::today()) {
                    $upgrade_request = 0;
                }
                $emailSetting = [];
                foreach ($emailSettingRaw as $key => $value) {
                    $emailSetting[$value['key']] = $value['value'];
                }
                $emailSetting['user'] = $user->toArray();
                $emailSetting['user']['price'] = $price;
                $emailSetting['user']['period'] = $period;
                $emailSetting['user']['domain_id'] = $website->id;
                $emailSetting['user']['domain'] = $website->domain;
                if (!$upgrade_request) {
                    EmailHelpers::sendMailToAdmin($emailSetting);
                    if (!$website->upgrade_request) {
                        $website->upgrade_request = 1;
                        $website->save();
                    }
                }
                $emailSetting['email'] = $user['email'];
                EmailHelpers::sendMailToUser($emailSetting);
            }
            return json_encode(['status'=>'success', 'message' => 'Update successfully']);
        }
        return view('frontend.upgrade.index');
    }
}
