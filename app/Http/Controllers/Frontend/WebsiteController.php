<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Models\Config;
use Illuminate\Support\Facades\Auth;
use App\Http\Models\Website;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Frontend\BaseController;
use App\Http\Models\Settings;

class WebsiteController extends BaseController
{

    protected  $ruleForm = [
        'domain' => 'required|unique:websites',
        'chat_id' => 'required'
    ];

    public function index() {
        $listWebsite = Website::where(['user_id'=>Auth::id()])->orderBy('id', 'DESC')->paginate(20);
        $user = Auth::user()->toArray();
        $baseUrl = Config::getConfigByKey('BASE_URL')->value;
        $webLeft = Auth::user()->domain_quantity - count($listWebsite);
        $settingModel = new Settings();
        $sellOption = $settingModel->getPublicSettingsWithKey('upgrade', 'sell_option')['value'];
        $sellOptionArr = preg_split("/\r\n/", $sellOption);
        $detailSellOption = [];
        foreach ($sellOptionArr as $key => $value) {
            $optionSplit = preg_split("/\s*[\:\-]\s*/", $value);
            $detailSellOption[] = [
                'content' => $value,
                'price' => $optionSplit[1],
                'period' => $optionSplit[0]
            ];
        };
        return view(
            'frontend.website.index',
            [
                'user' => $user,
                'list' => $listWebsite,
                'baseUrl' => $baseUrl,
                'webLeft'=> $webLeft,
                'detailSellOption' => $detailSellOption
            ]
        );
    }

    public function create(Request $request, Response $response) {
        $listWebsite = Website::where(['user_id'=>Auth::id()])->orderBy('id', 'DESC')->paginate(20);
        $webLeft = Auth::user()->domain_quantity - count($listWebsite);
        if ($webLeft <= 0) {
            return redirect()->route('website.index')->withErrors(['limited'=>'Số lượng website đã đạt giới hạn']);
        }
        return view('frontend.website.edit', ['data'=>[]]);
    }

    public function update($id, Request $request, Response $response) {
        $web = Website::find($id);
        if (!$web) {
            return back()->with('error','Không tồn tại website!');
        }
        return view('frontend.website.edit', ['data'=>$web]);
    }

    public function saveNew(Request $request, Response $response) {
        $web = new Website();
        $web->fill($request->all());
        $web->email = Auth::user()->email;
        $web->user_id = Auth::id();
        $validator = Validator::make($request->all(), $this->ruleForm);
        if ($validator->fails())
        {
            return view('frontend.website.edit', ['data'=>$web])->withErrors($validator);
            // The given data did not pass validation
        }
        $web->createdAt = Carbon::now();
        $web->updatedAt = Carbon::now();
        $web->save();
        return redirect()->route('website.update', ['id' => $web->id])->with('success', 'Tạo mới thành công');
    }

    public function store($id, Request $request, Response $response) {
        $web = Website::find($id);
        if (!$web) {
            return back()->with('error','Không tồn tại website!');
        }

        $rule = $this->ruleForm;
        if ($request->input('main_color')) {
            $rule['main_color'] = 'color';
        }
        $rule['domain'] = $rule['domain']. ',domain,' . $id;
        $validated = $request->validate($rule);
        $web->fill($request->all());
        $web->user_id = Auth::id();
        $web->save();
        return back()->with('success','Cập nhật thành công!');

    }

}
