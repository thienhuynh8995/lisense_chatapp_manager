<?php

namespace App\Http\Controllers\Frontend;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Frontend\BaseController;
use App\Http\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{
    protected  $ruleForm = [
        'email' => 'required|email|unique:users',
        'name' => 'required',
        'password' => 'required|min:6',
        'confirm_password' => 'required_with:password|same:password'
    ];

    public function update(Request $request, Response $response) {
        $id = Auth::user()->id;
        $user = User::find($id);
        return view('frontend.user.edit', ['data'=>$user]);
    }

    public function updateInfo(Request $request, Response $response) {
        $user = Auth::user();
        if (!empty($user->email)){
            return redirect('/');
        }
        if ($request->method() == 'POST'){
            $rule = $this->ruleForm;
            $rule['email'] = 'required|email|unique:users,email,NULL,id,provider_id,'.$user->provider_id.',provider,'.$user->provider;
            unset($rule['name'], $rule['password'], $rule['confirm_password']);
            $validated = $request->validate($rule);
            $oldUser = User::where(['email'=> $request->input('email'), 'provider'=>'web', 'provider_id' => '0'])->first();
            if ($oldUser) {
                $oldUser->provider = $user->provider;
                $oldUser->provider_id = $user->provider_id;
                $oldUser->save();
                Auth::logout();
                Auth::login($oldUser);
                User::where('id', $user->id)->delete();
            } else {
                $user->email = $request->input('email');
                $user->save();
            }
            return redirect('/');
        }
        return view('frontend.user.update_info');
    }

    public function store(Request $request, Response $response) {
        $user = User::find(Auth::id());
        if (!$user) {
            return back()->with('error','Không tồn tại người dùng!');
        }
        $rule = $this->ruleForm;
        unset($rule['email']);
        if (!$request->input('password') && !$request->input('confirm_password')) {
            unset($rule['password']);
            unset($rule['confirm_password']);
        }
        $validated = $request->validate($rule);
        $password = bcrypt($request->input('password'));

        $user->email = $request->input('email');
        $user->name = $request->input('name');
        $user->password = $request->input('password') !== null ? $password : $user->password;
        $user->save();
        return back()->with('success','Cập nhật thành công!');

    }
}
