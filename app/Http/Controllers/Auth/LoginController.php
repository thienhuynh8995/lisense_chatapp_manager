<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use function GuzzleHttp\json_encode;
use App\Http\Controllers\Admin\AppHelper;
use App\Http\Models\User;
use Illuminate\Support\Facades\View;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->setRedirectTo();
    }

    public function showLoginForm()
    {
        $guard = set_route_guard('web');
        $postUrl = '';
        if ($guard == 'admin') {
            $postUrl = 'admin/';
            View::share(['isAdminPage'=>true]);
        }
        config(['adminlte.login_url'=> $postUrl . 'login']);
        return view('auth.login');
    }

    public function username(){
        $login = request()->input('email');
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$field => $login]);
        return $field;
    }

    public function loginTelegram($prodiver){
        $checkValid = AppHelper::checkTelegramAuthorization(request()->all());
        $userSocial = request()->all();
        if ($checkValid['status'] == 'success'){
            $users =  User::where(['provider_id' => $userSocial['id'], 'provider'=>$prodiver])->first();
            if(!$users){
                $users = User::create([
                    'name'          => $userSocial['first_name'] . ' ' . $userSocial['last_name'],
                    'provider_id'   => $userSocial['id'],
                    'provider'      => $prodiver,
                ]);
            }
            Auth::login($users);
            return redirect('/');
        } else {
            Auth::logout();
            return redirect(route('login'))->withErrors(['auth' => $checkValid['message']]);
        }
        
    }

    public function logout() {
        Auth::logout();

        $guard = get_guard();
        $redirectTo = config('auth.guards.'.current(explode(".", $guard)).'.redirect_after_logout');
        if($redirectTo)
            return redirect()->route($redirectTo);

        return redirect('/');
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    protected function credentials(Request $request)
    {
        $role = get_guard('route') == 'admin' ? 2 : 1;
        return ['email' => $request->get('email'), 'password' => $request->get('password'), 'role' => $role];
    }

    /**
     * Return homepage for the user.
     *
     * @return Response
     */
    public function setRedirectTo()
    {
        $guard = get_guard();

        if (!empty($guard)) {
            $redirectTo = config('auth.guards.'.current(explode(".", $guard)).'.redirect_after_login');
            if($redirectTo)
                $redirectTo = route($redirectTo,[],false);
            else
                $redirectTo = current(explode(".", $guard));
            return $this->redirectTo = $redirectTo;
        }

        return $this->redirectTo = 'user';
    }
}
