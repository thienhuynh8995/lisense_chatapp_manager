<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Models\Settings;
use Illuminate\Support\Facades\App;
use AppHelper;
use App\Helpers\LayoutHelper;
use App\Traits\UploadTrait;
use App\Http\Requests\Settings as SettingsValidate;
use Illuminate\Support\Facades\Storage;
use App\Helpers\EmailHelpers;

class SettingsController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = request()->get('type') ? request()->get('type') : 'email';
        $settingModel = new Settings();
        $validSetting = $settingModel->checkSettingExists($type);
        $settings = $settingModel->where('type', $type)
        ->orderBy('updated_at', 'ASC')
        ->get()->toArray();
        
       
        return view('admin.settings.index')
            ->with(['settings' => $settings, 'type' => $type, 'validSetting' => $validSetting]);
    }

    /**
     * Edit template block.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(SettingsValidate $request)
    {
        $type = $request->get('type') !== null ? $request->get('type') : 'email';
        if ($request->isMethod('post')) {
            $settingsModel = new Settings();

            $post_data=$request->all();
            // update status settings
            $result = $settingsModel->addUpdateSettings($type, $post_data);
            if ($result['status']) {
                session()->flash('success', $result['message']);
            } else {
                session()->flash('error', $result['message']);
            }
        }
        return redirect(route('admin.settings.index')."?type=$type");
    }

    /**
     * Delete settings
     *
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $id = request()->get('id');
        $type = request()->get('type');
        if (!$id) {
            return json_encode(['status'=>'error', 'message'=>'Missing id']);
        }
        if (!$type) {
            return json_encode(['status'=>'error', 'message'=>'Missing type']);
        }
        Settings::where(['id'=> $id, 'type'=>$type])->delete();
        return json_encode(['status'=>'success', 'message'=>'Delete successfully']);
    }
}
