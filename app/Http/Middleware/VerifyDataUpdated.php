<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyDataUpdated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = $request->user();
        if($user !== null && ((get_guard('route')=='admin' && $user->role != 2) || (get_guard('route') == 'client' && $user->role != 1))){
            Auth::logout();
            return redirect(route('login'))->withErrors(['email' => __('auth.failed')]);
        } 

        if (empty($user->email)) {
            return redirect(route('user.update_info'));
        }
        return $next($request);
    }
}
