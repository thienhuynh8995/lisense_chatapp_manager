<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if($request->user() !== null && ((get_guard('route')=='admin' && $request->user()->role != 2) || (get_guard('route') == 'client' && $request->user()->role != 1))){
            Auth::logout();
            $routeGuard = get_guard('route') == 'client' ? '' : get_guard('route').'.';
            return redirect(route($routeGuard . 'login'))->withErrors(['email' => __('auth.failed')]);
        }

        return $next($request);
    }
}
