<?php

namespace App\Http\Controllers\Admin;

use App\Http\Models\Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AppHelper
{
    static public function checkTelegramAuthorization($auth_data) {
        $check_hash = $auth_data['hash'];
        unset($auth_data['hash']);
        $data_check_arr = [];
        foreach ($auth_data as $key => $value) {
            $data_check_arr[] = $key . '=' . $value;
        }
        sort($data_check_arr);
        $data_check_string = implode("\n", $data_check_arr);
        $secret_key = hash('sha256', env('TELEGRAM_TOKEN'), true);
        $hash = hash_hmac('sha256', $data_check_string, $secret_key);
        if (strcmp($hash, $check_hash) !== 0) {
            return ['status' => 'error', 'code'=> 401, 'message'=>'Invalid data'];
        }
        if ((time() - $auth_data['auth_date']) > 1) {
            return ['status' => 'error', 'code'=> 401, 'message'=>'Authentication is out date'];
        }
        return ['status' => 'success', 'message'=>'Successful authentication'];
    }
}
