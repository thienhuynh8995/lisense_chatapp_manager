<?php
/**
 ***************************************************************************
 * Language Helpers
 ***************************************************************************
 * Created: 2018/06/21
 * This is a helpers get, check language
 *
 *
 ***************************************************************************
 * @author: thuan<n_thuan@stagegroup.jp>
 ***************************************************************************
 */

namespace App\Helpers;

use Auth;
use Session;
use App\Jobs\SendEmail;
use App\Http\Models\Settings;

class EmailHelpers
{
    /**
     ***************************************************************************
     ***************************************************************************
     * @author: Thien Huynh<h_thien@stagegroup.jp>
     * @param: $aDatas
     * @return:
     *
     ***************************************************************************
     */
    public static function sendMailToUser(array $aDatas) {

        $sSubject = 'User Upgrade';
        // Check user email has existed or not
        if (array_key_exists("email",$aDatas)) {
            $aDatas['email_template'] = 'email.user_upgrade';
            $aDatas['email_subject'] = !empty($aDatas['email_subject']) ? $aDatas['email_subject'] : $sSubject;
            $aDatas['email_name'] = !empty($aDatas['email_name']) ? $aDatas['email_name'] : 'User new information';
            dispatch(new SendEmail($aDatas));
        }
    }

    /**
     ***************************************************************************
     ***************************************************************************
     * @author: Thien Huynh<h_thien@stagegroup.jp>
     * @param: $aDatas
     * @return:
     *
     ***************************************************************************
     */
    public static function sendMailToAdmin(array $aDatas) {

        $sSubject = 'New User Upgrade Announce';
        // Check user email has existed or not
        if (array_key_exists("email",$aDatas)) {
            $aDatas['email_template'] = 'email.user_upgrade_to_admin';
            $aDatas['email_subject'] = $sSubject;
            $aDatas['email_name'] = 'User upgrade announcement';
            dispatch(new SendEmail($aDatas));
        }
    }
}
