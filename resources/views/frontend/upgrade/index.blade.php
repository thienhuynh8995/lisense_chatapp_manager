{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('css')
@stop

@section('title', 'Dashboard')

@section('content_header')
    <div class="box-header">
        <h3>Nâng cấp tài khoản</h3>
    </div>
@stop

@section('content')

    <form method="post">
        @csrf
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p class="margin-bottom">Nâng cấp tài khoản để sử dụng nhiều tính năng hữu ích hơn
                    <br />
                    <br />
                    Ân vào "UPGRAGE NOW" để hệ thống gửi thông tin, phương thức thanh toán vào Email bạn ngay
                </p>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input class="btn btn-danger" type="submit" name="submit" value="UPGRADE NOW">
                </div>
            </div>
        </div>
    </form>


@stop


@section('js')
@stop
