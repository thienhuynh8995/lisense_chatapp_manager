{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('css')
@stop

@section('title', 'Dashboard')

@section('content_header')
    <h3>Danh sách website đã đăng ký</h3>
@stop

@section('content')
    <input type="hidden" id="base_url" value="{{$baseUrl}}">
    <div class="box">
        <div class="box-header">
            @if ($errors->has('limited'))
                <p class="text-danger">{{$errors->first('limited')}}</p>
            @endif
            <div class="pull-left">
                <a class="btn btn-block btn-primary {{$webLeft > 0 ? '' : 'disabled'}}" href="{{ $webLeft > 0 ? route('website.create') : '#'}}">Tạo mới ({{$webLeft}})</a>
            </div>

            <div class="box-tools">
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody>
                <tr>
                    <th>ID</th>
                    <th>Tên miền</th>
                    <th class="text-center">Số Điện Thoại</th>
                    <th class="text-center">Telegram Chat Id</th>
                    <th>Ngày hết hạn</th>
                    <th>Thao Tác</th>
                </tr>
                @forelse ($list as $index => $item)
                    <tr>
                        <td>{{$index + 1}}</td>
                        <td><a href="{{route('website.update', ['id'=>$item->id])}}">{{$item->domain}}</a></td>
                        <td class="text-center">{{$item->phone}}</td>
                        <td class="text-center">{{$item->chat_id}}</td>
                        <td>
                            <span class="{{ ($item->expire_date !== null && Carbon\Carbon::parse($item->expire_date) < Carbon\Carbon::today()) ? 'badge bg-red' : ''}}">
                                {{($item->expire_date === null) ? '' : Carbon\Carbon::parse($item->expire_date)->format('d/m/Y')}}
                            </span>
                        </td>
                        <td>
                            <a class="btn btn-primary" title="Sửa" href="{{route('website.update', ['id'=>$item->id])}}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="javascript:;" title="View Embed code" class="btn btn-success generate-code-chat" data-id="{{$item->id}}">
                                <i class="fa fa-file-code-o"></i>
                            </a>
                            <span class="btn delete-btn btn-danger" title="Xóa" data-id="{{$item->id}}">
                                <i class="fa fa-fw fa-close"></i>
                            </span>
                            <span class="btn upgrade-btn btn-info" title="Nâng cấp" data-name="{{$item->domain}}" data-id="{{$item->id}}">
                                <i class="fa fa-fw fa-upload"></i>
                            </span>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8">Hiện chưa có setting cho website nào cả</td>
                @endforelse
                </tbody>
            </table>
            {{ $list->links() }}
        </div>
        <!-- /.box-body -->
    </div>
    <!-- Modal -->

    <div class="modal fade" id="upgrade-info" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="upgrade-time-period" action="{{route('upgrade')}}">
                <div class="modal-content pull-left full-width">
                    <div class="modal-header pull-left full-width">
                        <a href="#" target="_blank"><h3 class="modal-title">Nâng cấp</h3>
                        </a>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body pull-left full-width">
                        @foreach ($detailSellOption as $item)
                            <p>
                                <input type="radio" name="upgrade-type" @if($loop->first) checked @endif data-price="{{ $item['price'] }}" data-period="{{ $item['period'] }}" value="{{$item['content']}}"> {{$item['content']}}
                            </p>
                        @endforeach
                        <input type="hidden" name="domain_id">
                        <input type="hidden" name="domain_name">
                    </div>
                    <div class="modal-footer pull-left full-width">
                        <button type="button"
                                class="btn btn-success btn-submit-popup pull-left" data-dismiss="modal">Đồng ý
                        </button>
                        <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="upgrade_payment" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content full-width">
                <div class="modal-header full-width">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body full-width text-center">
                    <div class="header">
                        <span class="domain">Domain: <b></b></span>
                        <span>Email: <b>{{ $user['email'] }}</b></span>
                        <span class="total">Total: <b></b></span>
                        <span class="period">Thời hạn: <b></b></span>
                        <span>Payment method: <b>Thanh toán qua MOMO</b></span>
                    </div>
                    <div class="payment_method">
                        <h3 class="credit_name">LAI NGOC LAM</h3>
                        <h4 class="phone">0902797922</h4>
                        <img width="150" src="{{ asset('assets/images/qr_code.png') }}" alt="QR Code">
                        <p class="text-left">
                            <b>Bước 1:</b> Quét mã QR hoặc NHẬP SỐ ĐIỆN THOẠI được ghi ở hình trên<br />
                            <b>Bước 2:</b> [NHẬP SỐ TIỀN] cần thanh toán cho đơn hàng<br />
                            <b>Bước 3:</b> Nhập [TÊN DOMAIN CẦN NÂNG CẤP] ở phần tin nhắn. Lưu ý: chỉ nhập [TÊN DOMAIN CẦN NÂNG CẤP] KHÔNG nhập gì khác (ví dụ: simplechat.vn)<br />
                            <b>Bước 4:</b> Chọn [XÁC NHẬN]. Vui lòng kiểm tra lại số điện thoại, số tiền và nội dung tin nhắn mà bạn sẽ chuyển trước khi nhấn [XÁC NHẬN]<br />
                        </p>
                        <p class="text-left">
                            Lưu ý:<br />
                            <i>Xin vui lòng kiểm tra kỹ [TÊN DOMAIN CẦN NÂNG CẤP] trước khi chuyển tiền (ví dụ: simplechat.vn)</i><br />
                            <i>Hệ thống sẽ tự động kích họat đơn và gửi thông báo tới email của bạn</i><br />
                            <i>Trong trường hợp thời gian chờ đợi quá lâu, hãy liên hệ với chúng tôi để được hỗ trợ sớm nhất</i>
                        </p>
                    </div>
                </div>
                <div class="modal-footer full-width">
                    <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="show-code-generated" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 800px">
            <form id="update-time-period">
                <div class="modal-content pull-left full-width">
                    <div class="modal-header pull-left full-width">
                        <a href="#" target="_blank"><h3 class="modal-title" id="generate-code-label">Embed Generated Code</h3>
                        </a>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body pull-left full-width">
                        <textarea id="code-generated-targeting" class="form-control disabled" rows="5"
                                  readonly></textarea>
                    </div>
                    <div class="modal-footer pull-left full-width">
                        <button id="copy-code-generated" type="button"
                                class="btn btn-success btn-submit-popup pull-left"
                                data-target="code-generated-targeting">Sao chép
                        </button>
                        <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Đóng</button>
                    </div>
            </form>
        </div>
    </div>
@stop


@section('js')
    <script>
        $(function () {
            $('.upgrade-btn').click(function () {
                let $this = $(this);
                let domain_id = $this.attr('data-id');
                let domain_name = $this.attr('data-name');
                $('#upgrade-time-period input[name="domain_id"]').val(domain_id);
                $('#upgrade-time-period input[name="domain_name"]').val(domain_name);
                $('#upgrade_payment .domain b').html(domain_name);
                $('#upgrade-info').modal('show');
            });

            
            $('#upgrade-info .btn-submit-popup').click(function(){
                let $checkedValue = $('#upgrade-info input[name="upgrade-type"]:checked');
                let price = $checkedValue.attr('data-price');
                let period = $checkedValue.attr('data-period');
                $('#upgrade_payment .total b').html(price);
                $('#upgrade_payment .period b').html(period);
                $('#upgrade_payment').modal('show');
                $.ajax({
                    type: "post",
                    url: "{{ route('upgrade') }}",
                    data: {
                        domain_id : $('#upgrade-time-period input[name="domain_id"]').val(),
                        domain_name : $('#upgrade-time-period input[name="domain_name"]').val(),
                        price: price,
                        period: period
                    },
                    dataType: "dataType",
                    success: function (response) {
                        
                    }
                });
            })

            $('.generate-code-chat').click(function () {
                var id = $(this).data('id');
                var url = '/api/website/' + id;
                var data;
                var requestDetail = $.get(url).done(function (res) {
                    if (res.code != 200) {
                        alert(res.message);
                    } else {
                        data = res.data;
                        var textCode = '\n';
                        var baseUrl = $('#base_url').val();
                        if(baseUrl[baseUrl.length-1] === "/") {
                            baseUrl = baseUrl.substring(0, baseUrl.length-1);
                        }
                        // textCode += '<script>\n';
                        // textCode += '    window.destinationId = ' + data.chat_id + ';\n';
                        // textCode += '    window.sourceServer = window.location.origin;\n';
                        // textCode += '    window.intergramCustomizations = {\n';
                        // if (data.title_closed) {
                        //     textCode += '        titleClosed: \'' + data.title_closed + '\',\n';
                        // } else {
                        //     textCode += '        titleClosed: \'Liên hệ\',\n';
                        // }

                        // if (data.title_open) {
                        //     textCode += '        titleOpen: \'' + data.title_open + '\',\n';
                        // } else {
                        //     textCode += '        titleOpen: \'Liên hệ\',\n';
                        // }

                        // if (data.intro_message) {
                        //     textCode += '        introMessage: \'' + data.intro_message + '\',\n';
                        // } else {
                        //     textCode += '        introMessage: \'Xin chào, chúng tôi có thể giúp gì cho bạn?\',\n';
                        // }

                        // if (data.placeholder_text) {
                        //     textCode += '        placeholderText: \'' + data.placeholder_text + '\',\n';
                        // } else {
                        //     textCode += '        placeholderText: \'Gửi tin nhắn...\',\n';
                        // }

                        // if (data.auto_response) {
                        //     textCode += '        autoResponse: \'' + data.auto_response + '\',\n';
                        // } else {
                        //     textCode += '        autoResponse: \'Đang kết nối tới tổng đài, xin vui lòng chờ trong giây lát\',\n';
                        // }

                        // if (data.auto_no_response) {
                        //     textCode += '        autoNoResponse: \'' + data.auto_no_response + '\',\n';
                        // } else {
                        //     textCode += '        autoNoResponse: \'Hiện tại đang không có nhân viên trực, xin vui lòng liên hệ lại sau!\',\n';
                        // }

                        // if (data.main_color) {
                        //     textCode += '        mainColor: "' + data.main_color + '", // Can be any css supported color \'red\', \'rgb(255,87,34)\', etc\n';
                        // } else {
                        //     textCode += '        mainColor: "#42659e", // Can be any css supported color \'red\', \'rgb(255,87,34)\', etc\n';
                        // }

                        // if (data.get_customer_info_text) {
                        //     textCode += '        getCustomerInfoText: \'' + data.auto_no_response + '\',\n';
                        // } else {
                        //     textCode += '        getCustomerInfoText: \'Xin vui lòng nhập thông tin của bạn để chúng tôi liên hệ!\',\n';
                        // }
                        // if (data.closed_chat_avatar_url) {
                        //     textCode += '        closedChatAvatarUrl: \'' + data.closed_chat_avatar_url + '\',\n';
                        // }
                        // if ($('#base_url').val()) {
                        //     textCode += '        requestServer: \'' + baseUrl + '\',\n';
                        // }

                        // textCode += '        alwaysUseFloatingButton: false // Use the mobile floating button also on large screens\n';
                        // textCode += '     };\n';
                        // textCode += '<\/script>\n';
                        textCode += '<script id="simpleChat-widget" src="' + $('#base_url').val() + 'js/widget.js"><\/script>\n';
                        $('#code-generated-targeting').text(textCode);
                        $('#show-code-generated').modal('show');
                    }
                });
            })
            $('#copy-code-generated').click(function () {
                var copyText = $('#code-generated-targeting');
                copyText.select();
                document.execCommand("copy");
                $.notify('Đã sao chép', {
                    className: 'success',
                    globalPosition: 'bottom right'
                })
            })
        })

        $('#update-time-period').submit(function (e) {
            e.preventDefault();
            data = getFormData($(this));
            url = '/api/website/' + data.current_website_id;
            if (data.periodValue <= 0) {
                alert('Dữ liệu thêm không đúng');
            } else {
                if (confirm('Bạn có chắc chắn muốn cập nhật')) {
                    $('.btn-submit-popup').addClass('disabled');
                    $.ajax({
                        url: url,
                        type: 'PUT',
                        data: data,
                        dataType: 'json'
                    }).done(function (res) {
                        if (res) {
                            if (res.code && res.code != 200) {
                                $('.btn-submit-popup').removeClass('disabled');
                                alert(res.message);
                            } else {
                                alert('Cập nhật thành công!');
                                location.reload();
                            }
                        }

                    })
                }
            }
            return false;
        })

        $('.delete-btn').click(function (e) {
            var id = $(this).data('id');
            url = '/api/website/' + id;
            if (confirm('Bạn có chắc chắn muốn xóa')) {
                $.ajax({
                    url: url,
                    type: 'DELETE'
                }).done(function (res) {
                    if (res) {
                        if (res.code && res.code != 200) {
                            alert(res.message);
                        } else {
                            alert('Xóa thành công!');
                            location.reload();
                        }
                    }

                })
            }
            return false;
        })
    </script>
@stop
