<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'Admin\AdminController@index');

Route::group(['prefix' => (set_route_guard('web')=='web'?'':set_route_guard('web'))], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::match(['get', 'post'], 'logout', 'Auth\LoginController@logout')->name('logout');
});
Route::middleware(['web'])->group(function () {
    Route::match(['get', 'post'], 'login/{provider}', 'Auth\LoginController@loginTelegram')->name('login_telegram');
    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');
    
    Route::match(['post', 'get'], 'upgrade', 'Frontend\UpgradeController@index')->name('upgrade');

    Route::prefix('website')->name('website.')->group(function () {
        Route::get('/', 'Frontend\WebsiteController@index')->name('index');
        Route::get('/create', 'Frontend\WebsiteController@create')->name('create');
        Route::post('/create', 'Frontend\WebsiteController@saveNew')->name('saveNew');
        Route::get('/update/{id}', 'Frontend\WebsiteController@update')->name('update');
        Route::put('/update/{id}', 'Frontend\WebsiteController@store')->name('store');
        Route::put('/update/{id}/token', 'Frontend\WebsiteController@updateToken')->name('updateToken');
    });

    Route::prefix('user')->name('user.')->group(function () {
        Route::get('/', 'Frontend\UserController@index')->name('index');
        Route::match(['get', 'post'], '/update-info', 'Frontend\UserController@updateInfo')->name('update_info');
        Route::get('/update', 'Frontend\UserController@update')->name('update');
        Route::put('/update', 'Frontend\UserController@store')->name('store');
        Route::put('/update/{id}/token', 'Frontend\UserController@updateToken')->name('updateToken');
    });
    Route::get('/', 'Frontend\WebsiteController@index')->name('home'); 
});
